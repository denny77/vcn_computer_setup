#############  #instantiate Variables   #####################33
#Fill in the following information  (will probably be made into a script parameter later)

#Ubuntu ISO filename
originaliso=$"ubuntu-18.04.2-desktop-amd64.iso"

#Ubuntu Version
mver=$"18"


#create working directories (Some folders probably not necessary)
sudo mkdir ~/custom-img
sudo mkdir ~/repositories/
sudo mkdir ~/repositories/custom-img/
sudo mkdir ~/repositories/custom-img/ISOs

#copy original ISO in the custom-img folder
cp ~/Downloads/$originaliso ~/custom-img

cd ~/custom-img

# Next, extract the contents of disc image.
sudo mkdir mnt
sudo mount -o loop $originaliso mnt
sudo mkdir extract
sudo rsync --exclude=/casper/filesystem.squashfs -a mnt/ extract

# Extract the filesystem with the following commands:
sudo unsquashfs mnt/casper/filesystem.squashfs
sudo mv squashfs-root edit
